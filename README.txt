Findings TestList.java
    Try with a LinkedList - does it make any difference?
        Differences: Overall LinkedList passed the tests quicker
                        testSet, testContainsAll, & testNonEmpty were sower in LinkedList
                        testRetainAll, testAddMultiple2,testRemoveObject, & testSizeEmpty were slower in ArrayList
    
    'list.remove(5);' What does this method do?
                            Removes the value 77 at index 5 from the list
                            
Findings TestIterator.java
    Try with a LinkedList - does it make any difference?
        Difference: testRemove takes longer in ArrayList; testNoneempty is longer in LinkedList
        
    What happens if you use list.remove(77)?  
        It attempts to remove a value at index 77 which is out of bounds 